/*Declarar variables en JavaScript*/

let numero=30;
var nombre="Josefina Gomez Palacio";
const PI=3.1416;
function arribaMouse(){

    let parrafo = document.getElementById('parrafo'); 
    parrafo.style.color = 'blue';
    parrafo.style.fontSize = '24px';
    parrafo.style.textAlign = 'justify';

}

function salirMouse (){
    let parrafo = document.getElementById('parrafo'); 
    parrafo.style.color = "red";
    parrafo.style.fontSize = "17px";
    parrafo.style.textAlign = "left";
}

function entrar(){

    let parrafo = document.getElementById('secpar'); 
    parrafo.style.color = '#3CE371';
    parrafo.style.fontSize = '45px';
    parrafo.style.textAlign = 'center';

}

function afuera(){
    let parrafo = document.getElementById('secpar'); 
    parrafo.style.color = "#E3A63C";
    parrafo.style.fontSize = "30px";
    parrafo.style.textAlign = "right";
}

function adentro(){

    let h1 = document.getElementById('titu'); 
    h1.style.color = '#953CE3';

}

function fuera(){
    let h1 = document.getElementById('titu'); 
    h1.style.color = "#000";
}

function limpiarParrafo(){
    let parrafo = document.getElementById('parrafo');
    parrafo.innerHTML ="";
}